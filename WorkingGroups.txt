
Main focus: precise spectroscopy for astrochemistry, astrophysics, atmospheric chemistry, remote sensing of greenhouse gases



WG3. Intermolecular interaction and potential energy surfaces: beyond gold standard methods  (toward platinum standards) 




WG2. „Molecules in motion” - collision dynamics and bound states calculations
kinetics of gas-phase reactions


WG1. Experimental spectroscopy and dynamics 
 - CRDS
 - Fourier spectroscopy
 - optical frequency combs
 - cold atoms and cold molecules



WG4. Applications - models of atmospheres, exoplanets, remote sensing, chemical reaction networks 
HITRAN community 
EXOMOL

